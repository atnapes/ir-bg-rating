
 function setPicker(){
    $(".dateInput").pDatepicker({
        format : "YYYY/MM/DD",
        navigator: {
            text: {
                btnNextText: ">",
                btnPrevText: "<"
            }
        }
    });
 };

// function getRequestParam(p){
//     return (window.location.search.match(new RegExp('[?&]' + p + '=([^&]+)')) || [, null])[1];
// };

// function reloadPageForDateSelection(){
//     var selectedDate = document.getElementById('datepicker').value;
//     var redirectLink = window.location.protocol + "//" + window.location.host + window.location.pathname + '?date=' + selectedDate;
//     console.log('Redirecting to: ' + redirectLink);
//     window.location.href = redirectLink;
// };
//
// function formatDate(input) {
//     var dateFormat = 'yyyy-mm-dd';
//     var parts = input.match(/(\d+)/g),
//         i = 0, fmt = {};
//     dateFormat.replace(/(yyyy|dd|mm)/g, function(part) { fmt[part] = i++; });
//
//     return new Date(parts[fmt['yyyy']], parts[fmt['mm']]-1, parts[fmt['dd']]);
// };

persian={0:'۰',1:'۱',2:'۲',3:'۳',4:'۴',5:'۵',6:'۶',7:'۷',8:'۸',9:'۹'};
function en2fa(el){
    if(el.nodeType==3){
        var list=el.data.match(/[0-9]/g);
        if(list!=null && list.length!=0){
            for(var i=0;i<list.length;i++)
                el.data=el.data.replace(list[i],persian[list[i]]);
        }
    }
    for(var i=0;i<el.childNodes.length;i++){
        en2fa(el.childNodes[i]);
    }
}

$(document).ready(function(){

//    setPicker();

    $( "#playersTable" ).DataTable({
        "language": {
            "decimal":        "/",
            "emptyTable":     "هیچ سابقه‌ای در جدول وجود ندارد",
            "info":           "نمایش _START_ تا _END_ از _TOTAL_ مورد",
            "info":           "_PAGE_ از _PAGES_ برگ",
            "infoEmpty":      "هیچ سابقه‌ای نیست.",
            "infoFiltered":   "(فیلتر شده از بین _MAX_ مورد)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "نمایش _MENU_ مورد",
            "loadingRecords": "در حال بارگذاری...",
            "processing":     "در حال پردازش...",
            "search":         "جستجو: ",
            "zeroRecords":    "۰ سابقه",
            "paginate": {
                "first":      "اولین",
                "last":       "آخرین",
                "next":       "بعدی",
                "previous":   "قبلی"
            },
            "aria": {
                "sortAscending":  ": برای مرتب کردن رو به بالا فعال کنید",
                "sortDescending": ": برای مرتب کردن رو به پایین فعال کنید"
            }
        }
    });
    en2fa($('body')[0]);
});