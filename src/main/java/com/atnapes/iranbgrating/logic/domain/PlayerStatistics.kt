package com.atnapes.iranbgrating.logic.domain

import com.atnapes.iranbgrating.data.entity.Game
import com.atnapes.iranbgrating.data.entity.Gender
import com.atnapes.iranbgrating.data.entity.Player
import java.math.BigInteger
import java.util.*

class PlayerStatistics {

    var playerId: BigInteger? = null
    var firstName: String? = null
    var lastName: String? = null
    var fatherName: String? = null
    var nin: String? = null
    var birthDate: Date? = null
        private set
    var cityOfBirth: String? = null
    var gender: Gender? = null
    var address: String? = null
    var contactNumber: String? = null
    var experience: Int = 0
    var games: List<Game>? = null
    private var rating: Float = 0.toFloat()

    val age: Int
        get() {
            val ageMillis = System.currentTimeMillis() - birthDate!!.time
            return Math.ceil(ageMillis.toDouble() / 1000.0 / 60.0 / 60.0 / 24.0 / 365.2422).toInt()
        }

    fun setBirthDate(birthDate: java.sql.Date) {
        this.birthDate = Date(birthDate.time)
    }

    fun getRating(): Int {
        return rating.toInt()
    }

    fun setRating(rating: Float) {
        this.rating = rating
    }

    fun setPlayer(player: Player) {
        this.playerId = player.playerId
        this.firstName = player.firstName
        this.lastName = player.lastName
        this.nin = player.nin
        this.birthDate = player.birthDate
        this.cityOfBirth = player.cityOfBirth
        this.gender = player.gender
        this.address = player.address
        this.contactNumber = player.contactNumber
        this.rating = player.rating
    }
}
