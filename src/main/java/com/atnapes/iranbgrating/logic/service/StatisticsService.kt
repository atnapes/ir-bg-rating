package com.atnapes.iranbgrating.logic.service

import com.atnapes.iranbgrating.data.entity.Game
import com.atnapes.iranbgrating.data.repository.EventRepository
import com.atnapes.iranbgrating.data.repository.GameRepository
import com.atnapes.iranbgrating.data.repository.PlayerRepository
import com.atnapes.iranbgrating.logic.domain.PlayerStatistics
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@Service
class StatisticsService {

    @Autowired private lateinit var mPlayerRepository: PlayerRepository

    @Autowired private lateinit var mGameRepository: GameRepository

    @Autowired private lateinit var mEventRepository: EventRepository

    //        Date date = this.createDateFromDateString(dateString);
    //        Iterable<Reservation> reservations = this.reservationRepository.findByDate(new java.sql.Date(date.getTime()));
    //        if (null != reservations) {
    //            reservations.forEach(reservation -> {
    //                Guest guest = this.guestRepository.findOne(reservation.getGuestId());
    //                if (null != guest) {
    //                    PlayerStatistics playerStatistics = roomReservationMap.get(reservation.getGameId());
    //                    playerStatistics.setDate(date);
    //                    playerStatistics.setFirstName(guest.getFirstName());
    //                    playerStatistics.setLastName(guest.getLastName());
    //                    playerStatistics.setGuestId(guest.getGameId());
    //                }
    //            });
    //        }
    //        List<PlayerStatistics> playerStatisticses = new ArrayList<>();
    //        for (Long roomId : roomReservationMap.keySet()) {
    //            playerStatisticses.add(roomReservationMap.get(roomId));
    //        }
    val playerList: List<PlayerStatistics>
        get() {

            val players = this.mPlayerRepository.findAllByOrderByRatingDesc()

            val playerStatisticsList = ArrayList<PlayerStatistics>()

            players.forEach { player ->

                val gamesPlayed = mGameRepository.findByFirstPlayer(player.playerId)

                val playerStatistics = PlayerStatistics()

                playerStatistics.setPlayer(player)
                playerStatistics.experience = calculateExperience(gamesPlayed)
                playerStatistics.games = gamesPlayed

                playerStatisticsList.add(playerStatistics)
            }
            return playerStatisticsList
        }

    private fun calculateExperience(gamesPlayed: List<Game>): Int {
        var experience = 0
        for (game in gamesPlayed)
            experience += game.point
        return experience
    }

    private fun createDateFromDateString(dateString: String?): Date? {
        var date: Date? = null
        if (null != dateString) {
            try {
                date = DATE_FORMAT.parse(dateString)
            } catch (pe: ParseException) {
                date = Date()
            }

        } else {
            date = Date()
        }
        return date
    }

    companion object {

        val DATE_FORMAT: DateFormat = SimpleDateFormat("yyyy-MM-dd")
    }
}
