package com.atnapes.iranbgrating.data

interface Prototype {
    fun clone(): Prototype
}
