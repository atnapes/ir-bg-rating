package com.atnapes.iranbgrating.data.repository

import com.atnapes.iranbgrating.data.entity.Player
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

import java.math.BigInteger

/**
 * Created by bahram on 6/8/17.
 */
@Repository
interface PlayerRepository : CrudRepository<Player, BigInteger> {

    @Query(value = "SELECT * FROM player", nativeQuery = true)
    fun findHame(): List<Player>

    fun findAllByOrderByRatingDesc(): List<Player>

    @Transactional
    override fun <S : Player?> save(p0: S): S
}
