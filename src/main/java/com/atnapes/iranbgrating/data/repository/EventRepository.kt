package com.atnapes.iranbgrating.data.repository

import com.atnapes.iranbgrating.data.entity.Event
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.math.BigInteger

/**
 * Created by bahram on 6/8/17.
 */
@Repository
interface EventRepository : CrudRepository<Event, BigInteger> {

    override fun findAll(): List<Event>

    fun findByEventId(eventId: BigInteger): List<Event>

    @Transactional
    override fun <S : Event?> save(p0: S): S
}
