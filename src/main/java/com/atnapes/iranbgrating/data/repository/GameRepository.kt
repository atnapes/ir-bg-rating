package com.atnapes.iranbgrating.data.repository

import com.atnapes.iranbgrating.data.entity.Game
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

import java.math.BigInteger

/**
 * Created by bahram on 6/8/17.
 */
@Repository
interface GameRepository : CrudRepository<Game, BigInteger> {

    override fun findAll(): List<Game>

    fun findByFirstPlayer(playerId: BigInteger?): List<Game>

}
