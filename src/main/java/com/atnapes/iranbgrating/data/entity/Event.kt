package com.atnapes.iranbgrating.data.entity

import com.atnapes.iranbgrating.util.Utils
import com.samanpr.jalalicalendar.JalaliCalendar
import org.thymeleaf.util.StringUtils
import java.math.BigInteger
import java.sql.Date
import java.util.logging.Logger
import javax.persistence.*

/**
 * Created by bahram on 6/8/17.
 */
@Entity
@Table(name = "EVENT")
class Event {

    @Id
    @Column(name = "EVENT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var eventId: BigInteger? = null

    @Column(name = "EVENT_NAME")
    var name: String? = null

    @Column(name = "START_DATE")
    var startDate: Date? = null
        private set

    @Column(name = "END_DATE")
    var endDate: Date? = null
        private set

    @Column(name = "LOCATION")
    var location: String? = null

    @Column(name = "TURNOUT")
    var turnout: Int = 0

    @Column(name = "ADDRESS")
    var address: String? = null

    @Column(name = "RESULTS")
    var results: String? = null

    @ManyToMany
    @JoinTable(name = "EVENT_PLAYER", joinColumns = [JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID")], inverseJoinColumns = [JoinColumn(name = "PLAYER_ID", referencedColumnName = "PLAYER_ID")])
    var players: List<Player>? = null

    val isValid: Boolean
        get() {
            val minDate = JalaliCalendar(1385, 1, 1).time
            return (null != name
                    && null != startDate && startDate!!.after(minDate)
                    && !StringUtils.isEmpty(location)
                    && turnout > 8
                    && null != players && players!!.size == turnout)
        }

    fun setStartDate(startDate: String) {
        this.startDate = Utils.getSqlDate(startDate)
    }

    fun setEndDate(endDate: String) {
        this.endDate = Utils.getSqlDate(endDate)
    }

    companion object {

        internal var log = Logger.getLogger(Event::class.java.name)
    }
}
