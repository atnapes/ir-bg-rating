package com.atnapes.iranbgrating.data.entity

import com.atnapes.iranbgrating.util.Utils
import java.math.BigInteger
import java.sql.Date
import javax.persistence.*

/**
 * Created by bahram on 6/8/17.
 */
@Entity
@Table(name = "PLAYER")
class Player {
    @Id
    @Column(name = "PLAYER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var playerId: BigInteger? = null

    @Column(name = "FIRST_NAME")
    var firstName: String? = null

    @Column(name = "LAST_NAME")
    var lastName: String? = null

    @Column(name = "FATHER_NAME")
    var fatherName: String? = null

    @Column(name = "NIN")
    var nin: String? = null
        set(nin) {
            if (null != nin) {
                field = Utils.toEnglishNumber(nin)
            }
        }

    @Column(name = "BIRTH_DATE")
    var birthDate: Date? = null
        private set

    @Column(name = "CITY_OF_BIRTH")
    var cityOfBirth: String? = null

    @Column(name = "GENDER")
    var gender: Gender? = null

    @Column(name = "ADDRESS")
    var address: String? = null
        set(address) {
            if (null != address) {
                field = Utils.toEnglishNumber(address)
            }
        }

    @Column(name = "CONTACT_NUMBER")
    var contactNumber: String? = null

    @Column(name = "RATING")
    var rating: Float = 0.toFloat()

    @ManyToMany
    @JoinTable(name = "EVENT_PLAYER", joinColumns = [JoinColumn(name = "PLAYER_ID", referencedColumnName = "PLAYER_ID")], inverseJoinColumns = [JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID")])
    private val events: List<Event>? = null

    fun setBirthDate(birthDate: String) {
        this.birthDate = Utils.getSqlDate(birthDate)
    }
}