package com.atnapes.iranbgrating.data.entity

import com.atnapes.iranbgrating.data.Prototype
import com.atnapes.iranbgrating.data.repository.EventRepository
import com.atnapes.iranbgrating.data.repository.PlayerRepository
import com.samanpr.jalalicalendar.JalaliCalendar
import java.math.BigInteger
import java.sql.Date
import javax.persistence.*

/**
 * Created by bahram on 6/8/17.
 */
@Entity
@Table(name = "GAME")
class Game : Prototype {

    @Id
    @Column(name = "GAME_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var gameId: BigInteger? = null

    @Column(name = "FIRST_PLAYER")
    var firstPlayer: BigInteger? = null

    @Column(name = "SECOND_PLAYER")
    var secondPlayer: BigInteger? = null

    @Column(name = "FIRST_PLAYER_RATING")
    var firstPlayerRating: Float = 0.toFloat()

    @Column(name = "SECOND_PLAYER_RATING")
    var secondPlayerRating: Float = 0.toFloat()

    @Column(name = "POINT")
    var point: Int = 0

    @Column(name = "FIRST_PLAYER_SCORE")
    var firstPlayerScore: Int = 0

    @Column(name = "SECOND_PLAYER_SCORE")
    var secondPlayerScore: Int = 0

    @Column(name = "TRANSCRIPT")
    var transcript: String? = null

    @Column(name = "_DATE")
    var date: Date? = null

    @Column(name = "EVENT")
    var event: BigInteger? = null

    @Column(name = "COMPETITION")
    var competition: String? = null

    fun isValid(playerRepository: PlayerRepository, eventRepository: EventRepository): Boolean {

        val minDate = JalaliCalendar(1385, 1, 1).time

        if (null != firstPlayer && playerRepository.existsById(firstPlayer)
                && null != secondPlayer && playerRepository.existsById(secondPlayer)
                && point > 0 && point <= 21
                && firstPlayerScore >= 0 && firstPlayerScore <= point
                && secondPlayerScore >= 0 && secondPlayerScore <= point
                && date != null && date!!.after(minDate)
                && (BigInteger("-1", 10).equals(event) || eventRepository.existsById(event))) {
            firstPlayerRating = playerRepository.findById(firstPlayer).get().rating
            secondPlayerRating = playerRepository.findById(secondPlayer).get().rating
            return true
        }
        return false
    }

    override fun clone(): Prototype {
        val game = Game()
        game.firstPlayer = firstPlayer
        game.secondPlayer = secondPlayer
        game.firstPlayerRating = firstPlayerRating
        game.secondPlayerRating = secondPlayerRating
        game.point = point
        game.firstPlayerScore = firstPlayerScore
        game.secondPlayerScore = secondPlayerScore
        game.transcript = transcript
        game.date = date
        game.event = event
        game.competition = competition
        return game
    }
}
