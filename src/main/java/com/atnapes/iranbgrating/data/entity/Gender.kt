package com.atnapes.iranbgrating.data.entity

/**
 * Created by bahram on 7/30/17.
 */
enum class Gender(val description: String) {

    FEMALE("زن"),
    MALE("مرد")

}