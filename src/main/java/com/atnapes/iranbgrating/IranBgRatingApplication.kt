package com.atnapes.iranbgrating

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@EnableAutoConfiguration
@ComponentScan

class IranBgRatingApplication {
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            runApplication<IranBgRatingApplication>(*args)
        }
    }
}