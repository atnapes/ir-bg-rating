package com.atnapes.iranbgrating.web.service

import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.io.IOException
import javax.servlet.http.HttpServletResponse

/**
 * Created by bahram on 6/11/17.
 */
@RestController
class IndexController : ErrorController {

    @RequestMapping(method = [RequestMethod.GET], path = [PATH])
    @Throws(IOException::class)
    fun handleError(response: HttpServletResponse) {
        response.sendRedirect("/players")
    }

    override fun getErrorPath(): String {
        return PATH
    }

    companion object {

        private const val PATH = "/error"
    }
}
