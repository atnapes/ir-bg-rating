package com.atnapes.iranbgrating.web.service

import com.atnapes.iranbgrating.data.entity.Event
import com.atnapes.iranbgrating.data.entity.Game
import com.atnapes.iranbgrating.data.repository.EventRepository
import com.atnapes.iranbgrating.data.repository.GameRepository
import com.atnapes.iranbgrating.logic.domain.PlayerStatistics
import com.atnapes.iranbgrating.logic.service.StatisticsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(ReservationServiceController.PATH_FIRST_PART)
class ReservationServiceController {

    @Autowired private lateinit var statisticsService: StatisticsService

    @Autowired private lateinit var gameRepository: GameRepository

    @Autowired private lateinit var eventRepository: EventRepository

    val allPlayers: List<PlayerStatistics>
        @RequestMapping(method = [RequestMethod.GET], value = [PATH_PLAYERS])
        get() = this.statisticsService.playerList

    val allGames: List<Game>
        @RequestMapping(method = [RequestMethod.GET], value = [PATH_GAMES])
        get() = gameRepository.findAll()

    val allEvents: List<Event>
        @RequestMapping(method = [RequestMethod.GET], value = [PATH_EVENTS])
        get() = eventRepository.findAll()

    companion object {
        const val PATH_FIRST_PART = "/api"
        private const val PATH_PLAYERS = "/players"
        private const val PATH_GAMES = "/games"
        private const val PATH_EVENTS = "/events"
    }
}
