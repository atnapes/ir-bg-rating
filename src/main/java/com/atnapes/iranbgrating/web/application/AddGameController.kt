package com.atnapes.iranbgrating.web.application

import com.atnapes.iranbgrating.data.entity.Game
import com.atnapes.iranbgrating.data.repository.EventRepository
import com.atnapes.iranbgrating.data.repository.GameRepository
import com.atnapes.iranbgrating.data.repository.PlayerRepository
import com.atnapes.iranbgrating.logic.service.StatisticsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.mvc.support.RedirectAttributes

/**
 * Created by bahram on 6/15/17.
 */
@Controller
@RequestMapping(AddGameController.URL)
class AddGameController @Autowired
constructor(private val mPlayerRepository: PlayerRepository,
            private val mEventRepository: EventRepository,
            private val mGameRepository: GameRepository) {

    @Autowired private lateinit var statisticsService: StatisticsService

    @RequestMapping(method = [RequestMethod.GET])
    fun getForm(model: Model): String {
        model.addAttribute("game", Game())
        //        List<PlayerStatistics> playerList = this.statisticsService.getPlayersList(dateString);
        //        model.addAttribute("playerList", playerList);

        return TEMPLATE
    }

    @RequestMapping(method = [RequestMethod.POST])
    @ResponseStatus(value = HttpStatus.FOUND)
    fun addPlayer(@ModelAttribute game: Game, model: Model, attrs: RedirectAttributes): String {

        if (game.isValid(mPlayerRepository, mEventRepository)) {
            val newGame = mGameRepository.save(game)
            attrs.addAttribute("success", true)
            attrs.addAttribute("newGame", newGame)
        } else {
            attrs.addAttribute("error", true)
//            logger.error("addGame invalid game: $game")
        }

        return "redirect:$URL"
    }

    companion object {

//        val logger = Logger.getLogger(AddGameController::class.java!!.getSimpleName())

        const val URL = "/admin/add-game"
        const val TEMPLATE = "form_add_game"
    }
}
