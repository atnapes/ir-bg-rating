package com.atnapes.iranbgrating.web.application

import com.atnapes.iranbgrating.data.entity.Gender
import com.atnapes.iranbgrating.data.entity.Player
import com.atnapes.iranbgrating.data.repository.PlayerRepository
import com.atnapes.iranbgrating.logic.service.StatisticsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.ModelAndView

/**
 * Created by bahram on 6/15/17.
 */
@Controller
@RequestMapping(AddPlayerController.UTL)
class AddPlayerController {

    @Autowired private lateinit var statisticsService: StatisticsService

    @Autowired private lateinit var playerRepository: PlayerRepository

    @RequestMapping(method = [RequestMethod.GET])
    fun getForm(model: Model): String {

        model.addAttribute("player", Player())
        model.addAttribute("genders", arrayOf(Gender.FEMALE, Gender.MALE))

        return TEMPLATE
    }

    @RequestMapping(method = [RequestMethod.POST])
    @ResponseStatus(HttpStatus.OK)
    fun addPlayer(@ModelAttribute player: Player, model: Model): ModelAndView {

        if (player.rating > 1500f) {
            player.rating = 1500f
        }

        val newPlayer = playerRepository.save(player)

        model.addAttribute("newPlayer", newPlayer)
        model.addAttribute("success", true)

        return ModelAndView("redirect:/admin/add-player")
    }

    companion object {

        const val TEMPLATE = "form_add_player"
        const val UTL = "/admin/add-player"
    }
}
