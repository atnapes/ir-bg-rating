package com.atnapes.iranbgrating.web.application

import com.atnapes.iranbgrating.logic.service.StatisticsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import java.util.*

@Controller
@RequestMapping(PlayerController.URL)
class PlayerController {

    @Autowired private lateinit var statisticsService: StatisticsService

    @RequestMapping(method = [RequestMethod.GET])
    fun getPlayersList(
            @RequestParam(value = "showNav", required = false) showNav: Boolean,
            @RequestParam(value = "offset", required = false) offset: Int?,
            @RequestParam(value = "limit", required = false) limit: Int?,
            //            @RequestParam(value="year", required = false) int year,
            model: Model): String {
        var offset = offset
        var limit = limit

        model.addAttribute("showNav", showNav)

        var playerList: Array<Any>? = null
        try {
            playerList = this.statisticsService.playerList.toTypedArray()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (null != offset || null != limit) {
            if (null == offset || offset < 0)
                offset = 0
            if (null == limit || limit <= 0)
                limit = playerList!!.size - offset
            val tmp = Arrays.copyOfRange(playerList!!, offset, offset + limit)
            playerList = tmp
        }

        model.addAttribute("playerList", playerList)
        return "players"
    }

    companion object {
        const val URL = "/players"
    }
}
