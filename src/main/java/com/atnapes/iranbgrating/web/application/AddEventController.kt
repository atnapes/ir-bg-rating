package com.atnapes.iranbgrating.web.application

import com.atnapes.iranbgrating.data.entity.Event
import com.atnapes.iranbgrating.data.repository.EventRepository
import com.atnapes.iranbgrating.logic.service.StatisticsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.servlet.mvc.support.RedirectAttributes

/**
 * Created by bahram on 7/16/17.
 */
@Controller
@RequestMapping(AddEventController.URL)
class AddEventController {

    @Autowired private lateinit var statisticsService: StatisticsService

    @Autowired private lateinit var eventRepository: EventRepository

    @RequestMapping(method = [RequestMethod.GET])
    @ResponseStatus(HttpStatus.OK)
    fun getForm(model: Model,
                @ModelAttribute(flashAttrLabel) flashAttribute: Any): String {

        model.addAttribute(flashAttribute)
        model.addAttribute("event", Event())

        return TEMPLATE
    }

    @RequestMapping(method = [RequestMethod.POST])
    @ResponseStatus(value = HttpStatus.FOUND)
    fun addEvent(@ModelAttribute event: Event, model: ModelMap, attributes: RedirectAttributes): String {

        if (event.isValid) {
            val newEvent = eventRepository.save(event)
            attributes.addAttribute(flashAttrLabel, false)
        } else {
            attributes.addAttribute(flashAttrLabel, true)
//            logger.error("addEvent invalid event: $event")
        }

        return "redirect:$URL"
    }

    companion object {

        const val URL = "/admin/add-event"
        const val TEMPLATE = "form_add_event"

//        internal var logger = Logger.getLogger(AddEventController::class.java!!.getSimpleName())

        private const val flashAttrLabel = "addSuccess"
    }
}
