package com.atnapes.iranbgrating.util

import java.sql.Date
import java.text.ParseException
import java.text.SimpleDateFormat

/**
 * Created by bahram on 7/31/17.
 */
object Utils {

    private val persianNumbers = arrayOf("۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹")

    private val englishNumbers = arrayOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")

    fun toPersianNumber(number: Int): String {
        return toPersianNumber(number.toString())
    }

    fun toPersianNumber(text: String): String {
        if (text.length == 0)
            return ""
        var out = ""
        val length = text.length
        for (i in 0 until length) {
            val c = text[i]
            if ('0' <= c && c <= '9') {
                val number = Integer.parseInt(c.toString())
                out += persianNumbers[number]
            } else if (c == '٫' || c == ',')
                out += '،'.toString()
            else
                out += c
        }
        return out
    }

    fun toEnglishNumber(number: Int): String {
        return toEnglishNumber(number.toString())
    }

    fun toEnglishNumber(text: String): String {
        if (text.length == 0)
            return ""
        var out = ""
        val length = text.length
        for (i in 0 until length) {
            val c = text[i]
            if ('۰' <= c && c <= '۹') {
                val number = Integer.parseInt(c.toString())
                out += englishNumbers[number]
            } else if (c == '٫' || c == '،')
                out += ','.toString()
            else
                out += c
        }
        return out
    }

    fun getSqlDate(birthDate: String): Date? {
        try {
            val dateFormat = SimpleDateFormat("YYYY/MM/DD")
            val parseDate = dateFormat.parse(birthDate)
            return Date(parseDate.time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return null
    }

}
