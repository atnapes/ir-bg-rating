# Iran Backgammon Statistics Service #

A system to store statistics of iranian backgammon players.
Presentation layers can be added in future.

---
#### Host ####
```
localhost:8080
```

#### HTTP endpoints ####

    METHOD  PATH                CONTENT-TYPE
    GET     /api/players        json
    GET     /api/games          json
    GET     /api/events         json

    GET     /			        index.html
    GET     /players	        html

    GET     /admin/add-player   html
    POST    /admin/add-player   html

    GET     /admin/add-game     html
    POST    /admin/add-game     html

    GET     /admin/add-event    html
    POST    /admin/add-event    html